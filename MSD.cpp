#include <iostream>
#include <queue>
#include <algorithm>
#include <vector>

using namespace std;


template<typename T>
void RadixSortMSD(vector<T>& v, int l, int r, int d = sizeof(T) * 8 - 1) {
    if ((d == 0) || (l >= r)) {
        return;
    }

    queue<T> q0, q1;
    for (size_t i = l; i < r; ++i) {
        if ((v[i] >> d) & 1) {
            q1.push(v[i]);
        }
        else {
            q0.push(v[i]);
        }
    }

    int q0_size = q0.size();
    int q1_size = q1.size();

    int j = l;
    while (!q1.empty()) {
        v[j] = q1.front();
        q1.pop();
        ++j;
    }

    while (!q0.empty()) {
        v[j] = q0.front();
        q0.pop();
        ++j;
    }

    RadixSortMSD(v, l, l + q1_size, d - 1);
    RadixSortMSD(v, r-q0_size,r, d - 1);

}

int main() {
    cout<<"Hello World!"<<endl; //7 8 92 -1 0 -3 8 2 -4 -55 -20 -3 40 -1
    int x;
    vector<int>arr;
    while(cin.peek()!='\n'){
        cin>>x;
        arr.push_back(x);
    }

    RadixSortMSD<int>(arr, 0, arr.size());

    cout<<"Pre rearrange result:\n";
    for (size_t i = 0; i < arr.size(); ++i) {
        cout << arr[i] << " ";
    }
    int index=0;
    for(int i=0;i<arr.size()-1;i++)
        if((arr[i]*arr[i+1])<0)
            index = i;
    if (index!=0) {
        reverse(arr.begin(), arr.begin() + index + 1);
        reverse(arr.begin() + index + 1, arr.end());
    }
    cout<<endl<<"Rearranged result:\n";
    for(int i=0;i<arr.size();i++)
        cout<<arr[i]<<" ";
    return 0;
}
