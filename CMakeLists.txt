cmake_minimum_required(VERSION 3.15)
project(LSD)

set(CMAKE_CXX_STANDARD 14)

add_executable(LSD LSD.cpp)
add_executable(MSD MSD.cpp)